# Documentation and Code base of JNTU GLUG.


This repository contains some part of work done by JNTU GLUG. This includes Glug Initiatives, Proposal reposrts, Knowledge docs and code files. These files represents a part of work done by JNTU-GLUG

This repository includes the files:

            1. Software Freedom Day Documentation. 
            2. Letter Request for campaigns.
            3. Proposal for community development sessions.
            4. Saturated saturdays.
            5. Eagle CAD details (Not open source, but knowledge about it is useful).
            6. Mozilla app Developmet.
            7. Boot camp details
            8. Generally Used Circuit board details.
            9. Modular watch design Proposal.
            10. Presentation: About Open Source Hardware.
            11. Smart Watch design Principles - Raw data.
            12. QuadCopter Design Proposal.
            13. Student Achivements.
            14. Visual Maths - A basic effort done  for visualising maths (Now the project is not in progress and is terminated).
            15. Students Achievements 
            16. Code:Contains part of code developed by GLUG team such as ECE web sites. 
            17. JNTUGLUG pptx : About
            18. Freedom Call Registration : Docs for assigning and registring client during spoorthi fest - Freedom box testing, 
            19. Activity Schedule : JNTU gLUG activity schedule.
            20. Arduino Bootloader - OTA approcah details.
            21. Basic Kits - For electronic Design.
            22. JntuGlug-Syllabus-Learnings : Syllabus to be learn/taught/discuss/collaborate.
            23. Chennai Flood : Funds collected for Chennnai Flood Disaster.
            24. Owncloud implementation- Not Yet documented here...
            25. Moodle Implementation - Not yet documented here..
            26. OpenWRT Implementation - Not Yet documented here... 


Core Contributions : 
         
            JNTUGLUG 2014-2016.
      
